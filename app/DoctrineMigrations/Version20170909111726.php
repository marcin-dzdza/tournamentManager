<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170909111726 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament (id INT AUTO_INCREMENT NOT NULL, manager_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, system INT NOT NULL, status INT NOT NULL, date_start DATETIME NOT NULL, date_end DATETIME DEFAULT NULL, INDEX IDX_BD5FB8D9783E3463 (manager_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament_membership (id INT AUTO_INCREMENT NOT NULL, team_id INT DEFAULT NULL, tournament_id INT DEFAULT NULL, date_start DATETIME NOT NULL, date_end DATETIME DEFAULT NULL, INDEX IDX_EC9F72DF296CD8AE (team_id), INDEX IDX_EC9F72DF33D1A3E7 (tournament_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, tournament_id INT DEFAULT NULL, level INT NOT NULL, status INT NOT NULL, date_start DATETIME DEFAULT NULL, date_end DATETIME DEFAULT NULL, comment LONGTEXT DEFAULT NULL COLLATE utf8_polish_ci, INDEX IDX_232B318C727ACA70 (parent_id), INDEX IDX_232B318C33D1A3E7 (tournament_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game_team (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, tournament_membership_id INT DEFAULT NULL, game_score VARCHAR(80) DEFAULT NULL COLLATE utf8_polish_ci, tournament_score INT DEFAULT NULL, resultCode INT DEFAULT NULL, INDEX IDX_2FF5CA33E48FD905 (game_id), INDEX IDX_2FF5CA339D2D2270 (tournament_membership_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament ADD CONSTRAINT FK_BD5FB8D9783E3463 FOREIGN KEY (manager_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tournament_membership ADD CONSTRAINT FK_EC9F72DF296CD8AE FOREIGN KEY (team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE tournament_membership ADD CONSTRAINT FK_EC9F72DF33D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C727ACA70 FOREIGN KEY (parent_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C33D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id)');
        $this->addSql('ALTER TABLE game_team ADD CONSTRAINT FK_2FF5CA33E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE game_team ADD CONSTRAINT FK_2FF5CA339D2D2270 FOREIGN KEY (tournament_membership_id) REFERENCES tournament_membership (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_membership DROP FOREIGN KEY FK_EC9F72DF33D1A3E7');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C33D1A3E7');
        $this->addSql('ALTER TABLE game_team DROP FOREIGN KEY FK_2FF5CA339D2D2270');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C727ACA70');
        $this->addSql('ALTER TABLE game_team DROP FOREIGN KEY FK_2FF5CA33E48FD905');
        $this->addSql('DROP TABLE tournament');
        $this->addSql('DROP TABLE tournament_membership');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE game_team');
    }
}
