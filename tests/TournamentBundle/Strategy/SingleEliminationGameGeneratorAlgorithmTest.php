<?php
namespace Tests\TournamentBundle\Strategy;

use TournamentBundle\Strategy\SingleEliminationGameGeneratorAlgorithm;
use PHPUnit\Framework\TestCase;
use TournamentBundle\Entity\Game;

class SingleEliminationGameGeneratorAlgorithmTest extends TestCase
{
    const TEAM_PREFIX = 'team ';

    public function testGenerateThrowsWhenLessThanTwoTeams()
    {
        $teams = $this->createTeams(1);
        $this->expectException(\Exception::class);
        $generator = new SingleEliminationGameGeneratorAlgorithm($teams);
    }

    /**
     * @dataProvider getRoundCountProvider
     */
    public function testGetRoundCountReturnsCorrectNumberOfRounds($teamCount, $expectedRounds)
    {
        $teams = $this->createTeams($teamCount);
        $generator = new SingleEliminationGameGeneratorAlgorithm($teams);
        $generator->generateGames();
        $this->assertEquals($expectedRounds, $generator->getRoundCount());
    }

    public function getRoundCountProvider()
    {
        return [
            '2 teams' => [2, 1],
            '3 teams' => [3, 2],
            '4 teams' => [4, 2],
            '5 teams' => [5, 3],
            '6 teams' => [6, 3],
            '7 teams' => [7, 3],
            '8 teams' => [8, 3],
            '9 teams' => [9, 4],
        ];
    }

    public function testGenerateWith2TeamsReturnsGameWith0ChildNodes()
    {
        $teams = $this->createTeams(2);
        $generator = new SingleEliminationGameGeneratorAlgorithm($teams);
        $game = $generator->generateGames();

        $this->assertInstanceOf(Game::class, $game);
        $this->assertCount(0, $game->getChildren());
        $gameTeams = $game->getGameTeams()->getValues();
        $this->assertCount(2, $gameTeams);
        $this->assertTrue(in_array($this->getTeamName(0), $gameTeams));
        $this->assertTrue(in_array($this->getTeamName(1), $gameTeams));
    }

    public function testGenerateWith4TeamsReturnsGameWith1ChildNode()
    {
        $teams = $this->createTeams(4);
        $generator = new SingleEliminationGameGeneratorAlgorithm($teams);
        $game = $generator->generateGames();

        $this->assertCount(2, $game->getChildren());
        $this->assertCount(0, $game->getGameTeams()->getValues());

        foreach ($game->getChildren() as $childGame) {
            $this->assertCount(0, $childGame->getChildren());
            $gameTeams = $childGame->getGameTeams()->getValues();
            $this->assertCount(2, $gameTeams);
        }
    }

    public function testGenerateWith8TeamsReturnsGameWith2ChildNodes()
    {
        $teams = $this->createTeams(8);
        $generator = new SingleEliminationGameGeneratorAlgorithm($teams);
        $game = $generator->generateGames();

        $this->assertCount(2, $game->getChildren());
        $this->assertCount(0, $game->getGameTeams()->getValues());

        foreach ($game->getChildren() as $childGame) {
            $this->assertCount(2, $childGame->getChildren());
            $this->assertCount(0, $game->getGameTeams()->getValues());

            foreach ($childGame->getChildren() as $grandChildGame) {
                $this->assertCount(0, $grandChildGame->getChildren());
                $gameTeams = $grandChildGame->getGameTeams()->getValues();
                $this->assertCount(2, $gameTeams);
            }
        }
    }

    /**
     * @dataProvider getByeCountProvider
     */
    public function testGetByeCountReturnsCorrectNumberOfByes($teamCount, $expectedByes)
    {
        $teams = $this->createTeams($teamCount);
        $generator = new SingleEliminationGameGeneratorAlgorithm($teams);
        $generator->generateGames();
        $this->assertEquals($expectedByes, $generator->getByeCount());
    }

    public function getByeCountProvider()
    {
        return [
            '2 teams' => [2, 0],
            '3 teams' => [3, 1],
            '4 teams' => [4, 0],
            '5 teams' => [5, 3],
            '6 teams' => [6, 2],
            '7 teams' => [7, 1],
            '8 teams' => [8, 0],
            '9 teams' => [9, 7],
            '10 teams' => [10, 6],
            '11 teams' => [11, 5],
            '12 teams' => [12, 4],
            '13 teams' => [13, 3],
            '14 teams' => [14, 2],
            '15 teams' => [15, 1],
            '16 teams' => [16, 0]
        ];
    }

    /**
     * @dataProvider getDoubleByeGameCountProvider
     */
    public function testGetDoubleByeGameCountReturnsCorrectNumberOfGames($teamCount, $expectedDoubleByeGameCount)
    {
        $teams = $this->createTeams($teamCount);
        $generator = new SingleEliminationGameGeneratorAlgorithm($teams);
        $generator->generateGames();
        $this->assertEquals($expectedDoubleByeGameCount, $generator->getDoubleByeGameCount());
    }

    public function getDoubleByeGameCountProvider()
    {
        return [
            '2 teams' => [2, 0],
            '3 teams' => [3, 0],
            '4 teams' => [4, 0],
            '5 teams' => [5, 1],
            '6 teams' => [6, 0],
            '7 teams' => [7, 0],
            '8 teams' => [8, 0],
            '9 teams' => [9, 3],
            '10 teams' => [10, 2],
            '11 teams' => [11, 1],
            '12 teams' => [12, 0],
            '13 teams' => [13, 0],
            '14 teams' => [14, 0],
            '15 teams' => [15, 0],
            '16 teams' => [16, 0]
        ];
    }

    public function testGenerateWith3TeamsReturnsGameWith1ChildGameAnd1Bye()
    {
        $teams = $this->createTeams(3);
        $generator = new SingleEliminationGameGeneratorAlgorithm($teams);
        $game = $generator->generateGames();

        $this->assertCount(1, $game->getChildren());
        $this->assertCount(1, $game->getGameTeams()->getValues());

        foreach ($game->getChildren() as $childGame) {
            $this->assertCount(0, $childGame->getChildren());
            $gameTeams = $childGame->getGameTeams()->getValues();
            $this->assertCount(2, $gameTeams);
        }
    }

    public function testGenerateWith5TeamsReturnsCorrectNumberOfChildrenAndByes()
    {
        $teams = $this->createTeams(5);
        $generator = new SingleEliminationGameGeneratorAlgorithm($teams);
        $game = $generator->generateGames();

        $this->assertCount(2, $game->getChildren());
        $this->assertCount(0, $game->getGameTeams()->getValues());

        $children = $game->getChildren();

        $child1 = $children[0];
        $this->assertCount(0, $child1->getChildren());
        $this->assertCount(2, $child1->getGameTeams()->getValues());

        $child2 = $children[1];
        $this->assertCount(1, $child2->getChildren());
        $this->assertCount(1, $child2->getGameTeams()->getValues());

        $grandChild = $child2->getChildren()[0];
        $this->assertCount(0, $grandChild->getChildren());
        $this->assertCount(2, $grandChild->getGameTeams()->getValues());
    }

    private function createTeams(int $number)
    {
        $teams = [];
        for ($i = 0; $i < $number; $i++) {
            $teams[] = self::TEAM_PREFIX . $i;
        }
        return $teams;
    }

    private function getTeamName(int $index)
    {
        return self::TEAM_PREFIX . $index;
    }
}