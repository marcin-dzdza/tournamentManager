<?php

namespace TournamentBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use TournamentBundle\Entity\Tournament;
use TournamentBundle\Entity\Team;
use TournamentBundle\Entity\Game;
use TournamentBundle\Entity\TournamentMembership;
use TournamentBundle\Factory\GameGeneratorFactory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Tournament controller.
 *
 * @Route("tournament")
 */
class TournamentController extends Controller
{
    /**
     * Lists all tournament entities.
     *
     * @Route("/", name="tournament_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tournaments = $em->getRepository('TournamentBundle:Tournament')->findAll();

        $managedTeams = $this->getUserManagedTeams();

        return $this->render('tournament/index.html.twig', array(
            'tournaments' => $tournaments,
            'managedTeams' => $managedTeams
        ));
    }

    /**
     * Lists all tournaments managed by logged user.
     *
     * @Route("/managed", name="tournament_managed")
     * @Method("GET")
     */
    public function allManagedTournamentsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $tournaments = $em->getRepository('TournamentBundle:Tournament')->findManagedTournaments($user);

        $managedTeams = $this->getUserManagedTeams();

        return $this->render('tournament/managed.html.twig', array(
            'tournaments' => $tournaments,
            'managedTeams' => $managedTeams
        ));
    }

    /**
     * Lists all tournaments joined by team.
     *
     * @Route("/joined/{id}", name="tournament_joined")
     * @Method("GET")
     */
    public function allJoinedTournamentsAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $team = $em->getRepository('TournamentBundle:Team')->findOneById($id);
        if (!$team) {
            return $this->redirectToRoute('tournament_index', array());
        }

        $tournaments = $em->getRepository('TournamentBundle:Tournament')->findJoinedTournaments($team);
        $managedTeams = $this->getUserManagedTeams();

        return $this->render('tournament/joined.html.twig', array(
            'tournaments' => $tournaments,
            'managedTeams' => $managedTeams,
            'team' => $team
        ));
    }

    /**
     * Creates a new tournament entity.
     *
     * @Route("/new", name="tournament_new")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function newAction(Request $request)
    {
        $tournament = new Tournament();
        $form = $this->createForm('TournamentBundle\Form\TournamentType', $tournament);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tournament->setManager($this->getUser());
            $tournament->setDateStart(new \DateTime());
            $tournament->setStatus(Tournament::STATUS_RECRUITING);
            $em = $this->getDoctrine()->getManager();
            $em->persist($tournament);
            $em->flush();

            return $this->redirectToRoute('tournament_show', array('id' => $tournament->getId()));
        }

        return $this->render('tournament/new.html.twig', array(
            'tournament' => $tournament,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tournament entity.
     *
     * @Route("/{id}", name="tournament_show")
     * @Method("GET")
     */
    public function showAction(Tournament $tournament)
    {
        $deleteForm = $this->createDeleteForm($tournament);

        $managedTeams = $this->getUserManagedTeams();

        return $this->render('tournament/show.html.twig', array(
            'tournament' => $tournament,
            'delete_form' => $deleteForm->createView(),
            'managedTeams' => $managedTeams
        ));
    }

    /**
     * Finds and displays a tree diagram of matches in one tournament.
     *
     * @Route("/{id}/tree", name="tournament_tree")
     * @Method("GET")
     */
    public function treeAction(Tournament $tournament)
    {
        $nodeStructureAsJson = '';

        if ($tournament->getStatus() == Tournament::STATUS_STARTED) {
            $finalGame = $tournament->getFirstGameNodes()[0];
            $nodeStructure = $this->generateTreeFromGame($finalGame);
            $nodeStructureAsJson = json_encode($nodeStructure);
        }

        return $this->render('tournament/tree.html.twig', array(
            'tournament' => $tournament,
            'nodeStructure' => $nodeStructureAsJson
        ));
    }

    /**
     * Finds and displays all games in this tournament
     *
     * @Route("/{id}/games", name="tournament_games")
     * @Method("GET")
     */
    public function gamesAction(Tournament $tournament)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('TournamentBundle:Game');
        $games = $repository->findTournamentGames($tournament);

        return $this->render('tournament/games.html.twig', array(
            'tournament' => $tournament,
            'games' => $games
        ));
    }

    private function generateTreeFromGame(Game $game)
    {
        $nodeStructure = [];

        $defaultLabel = '?';
        $label = null !== $game->getWinner()
            ? $game->getWinner()->getTournamentMembership()->getTeam()->getName()
            : $defaultLabel;
        $nodeStructure['text'] = ['name' => 'game', 'desc' => $label];
        $nodeStructure['link'] = ['href' => $this->generateUrl('game_show', array('id' => $game->getId()))];
        $nodeStructure['children'] = [];
        foreach ($game->getChildren() as $childGame) {
            $nodeStructure['children'][] = $this->generateTreeFromGame($childGame);
            $winnerGameTeam = $childGame->getWinner();
            if ($winnerGameTeam) {
                $winnerTournamentMembership = $winnerGameTeam->getTournamentMembership();
                $alreadyIncludedTms[$winnerTournamentMembership->getId()] = $winnerTournamentMembership;
            }
        }
        $availableTms = [];
        foreach ($game->getGameTeams() as $gameTeam) {
            $availableTm = $gameTeam->getTournamentMembership();
            if (!isset($alreadyIncludedTms[$availableTm->getId()])) {
                $availableTms[] = $availableTm;
            }
        }

        while (count($nodeStructure['children']) < 2 && count($availableTms > 0)) {
            $singleTeamNode = $this->generateTeamNode(array_shift($availableTms));
            $nodeStructure['children'][] = $singleTeamNode;
        }

        return $nodeStructure;
    }

    private function generateTeamNode(TournamentMembership $tm)
    {
        $singleTeamNode['text'] = ['name' => 'team', 'desc' => $tm->getTeam()->getName()];
        $teamId = $tm->getTeam()->getId();
        $singleTeamNode['link'] = ['href' => $this->generateUrl('team_show', array('id' => $teamId))];

        return $singleTeamNode;
    }

    /**
     * Displays a form to edit an existing tournament entity.
     *
     * @Route("/{id}/edit", name="tournament_edit")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function editAction(Request $request, Tournament $tournament)
    {
        $userIsTournamentManager = $this->getUser()->getId() === $tournament->getManager()->getId();
        if (!$userIsTournamentManager) {
            return $this->redirectToRoute('tournament_show', array('id' => $tournament->getId()));
        }

        $deleteForm = $this->createDeleteForm($tournament);
        $editForm = $this->createForm('TournamentBundle\Form\TournamentType', $tournament);

        if ($tournament->getStatus() >= Tournament::STATUS_STARTED) {
            $editForm->remove('system');
        }

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('tournament_show', array('id' => $tournament->getId()));
        }

        return $this->render('tournament/edit.html.twig', array(
            'tournament' => $tournament,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Starts a tournament
     *
     * @Route("/{id}/start", name="tournament_start")
     * @Method({"GET"})
     * @Security("has_role('ROLE_USER')")
     */
    public function startAction(Request $request, Tournament $tournament)
    {
        $userIsTournamentManager = $this->getUser()->getId() === $tournament->getManager()->getId();
        if (!$userIsTournamentManager) {
            return $this->redirectToRoute('tournament_show', array('id' => $tournament->getId()));
        }

        if ($tournament->getStatus() != Tournament::STATUS_RECRUITING) {
            return $this->redirectToRoute('tournament_show', array('id' => $tournament->getId()));
        }

        $em = $this->getDoctrine()->getManager();
        $game = $this->generateGames($tournament);
        $em->persist($game);
        $tournament->setStatus(Tournament::STATUS_STARTED);
        $em->persist($tournament);
        $em->flush();

        return $this->redirectToRoute('tournament_games', array('id' => $tournament->getId()));
    }

    private function generateGames(Tournament $tournament)
    {
        $gameGenerator = GameGeneratorFactory::create($tournament);
        $games = $gameGenerator->generateGames($tournament);
        return $games;
    }

    /**
     * Deletes a tournament entity.
     *
     * @Route("/{id}", name="tournament_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteAction(Request $request, Tournament $tournament)
    {
        $userIsTournamentManager = $this->getUser()->getId() === $tournament->getManager()->getId();
        if (!$userIsTournamentManager) {
            return $this->redirectToRoute('tournament_show', array('id' => $tournament->getId()));
        }

        $form = $this->createDeleteForm($tournament);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tournament);
            $em->flush();
        }

        return $this->redirectToRoute('tournament_index');
    }

    /**
     * Creates a form to delete a tournament entity.
     *
     * @param Tournament $tournament The tournament entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Tournament $tournament)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tournament_delete', array('id' => $tournament->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @Route("/{tournamentId}/join/{teamId}", name="tournament_join")
     * @Method({"GET"})
     * @Security("has_role('ROLE_USER')")
     */
    public function joinTournamentAction($tournamentId, $teamId)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('TournamentBundle:Tournament');
        $tournamentToJoin = $repository->findOneById($tournamentId);

        if ($tournamentToJoin->getStatus() != Tournament::STATUS_RECRUITING) {
            return $this->redirectToRoute('tournament_show', array('id' => $tournamentId));
        }

        $managedTeams = $this->getUserManagedTeams();
        $userIsTeamManager = false;
        foreach ($managedTeams as $managedTeam) {
            if ($teamId == $managedTeam->getId()) {
                $userIsTeamManager = true;
            }
        }

        if (!$userIsTeamManager) {
            return $this->redirectToRoute('tournament_show', array('id' => $tournamentId));
        }

        $repository = $em->getRepository('TournamentBundle:Team');
        $team = $repository->findOneById($teamId);
        $teamAlreadyJoined = false;
        foreach ($team->getTournamentMemberships() as $tournamentMembership) {
            if ($tournamentMembership->getTournament()->getId() == $tournamentId) {
                $teamAlreadyJoined = true;
            }
        }

        if ($teamAlreadyJoined) {
            return $this->redirectToRoute('tournament_show', array('id' => $tournamentId));
        }

        $newTournamentMembership = new TournamentMembership();
        $newTournamentMembership->setTeam($team);
        $newTournamentMembership->setTournament($tournamentToJoin);
        $newTournamentMembership->setDateStart(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($newTournamentMembership);
        $em->flush();

        return $this->redirectToRoute('tournament_show', array('id' => $tournamentToJoin->getId()));
    }

    private function getUserManagedTeams()
    {
        $managedTeams = [];
        $user = $this->getUser();
        if ($user) {
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('TournamentBundle:Team');
            $managedTeams = $repository->findByManager($user);
        }

        return $managedTeams;
    }
}
