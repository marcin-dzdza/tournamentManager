<?php

namespace TournamentBundle\Controller;

use TournamentBundle\Entity\Game;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

//related to forms
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use TournamentBundle\Repository\GameTeamRepository;
use TournamentBundle\Form\GameTeamType;
use TournamentBundle\Entity\GameTeam;

/**
 * Game controller.
 *
 * @Route("game")
 */
class GameController extends Controller
{
    /**
     * Lists all game entities.
     *
     * @Route("/", name="game_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $games = $em->getRepository('TournamentBundle:Game')->findAll();

        return $this->render('game/index.html.twig', array(
            'games' => $games,
        ));
    }

//    /**
//     * Creates a new game entity.
//     *
//     * @Route("/new", name="game_new")
//     * @Method({"GET", "POST"})
//     */
//    public function newAction(Request $request)
//    {
//        $game = new Game();
//        $form = $this->createForm('TournamentBundle\Form\GameType', $game);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($game);
//            $em->flush();
//
//            return $this->redirectToRoute('game_show', array('id' => $game->getId()));
//        }
//
//        return $this->render('game/new.html.twig', array(
//            'game' => $game,
//            'form' => $form->createView(),
//        ));
//    }

    /**
     * Finds and displays a game entity.
     *
     * @Route("/{id}", name="game_show")
     * @Method("GET")
     */
    public function showAction(Game $game)
    {

        return $this->render('game/show.html.twig', array(
            'game' => $game
        ));
    }

    /**
     * Displays a form to edit an existing game entity.
     *
     * @Route("/{id}/edit", name="game_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Game $game)
    {
        $form = $this->createEditForm($game);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->optionallySetWinner($form, $game);
            $this->optionallyUpdateNextGames($form, $game);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('game_show', array('id' => $game->getId()));
        }

        return $this->render('game/edit.html.twig', array(
            'game' => $game,
            'edit_form' => $form->createView()
        ));
    }

    private function createEditForm(Game $game)
    {
        $form = $this->createFormBuilder($game)
            ->add('winner', EntityType::class, array(
                'mapped' => false,
                'class' => 'TournamentBundle:GameTeam',
                'choice_label' => function ($gameTeam) {
                    return $gameTeam->getTournamentMembership()->getTeam()->getName();
                },
                'query_builder' => function(GameTeamRepository $er) use ($game) {
                    return $er->createQueryBuilder('gt')
                        ->where('gt.game = ?1')
                        ->setParameter(1, $game);
                }
            ))
            ->add('gameTeams', CollectionType::class, array(
                'entry_type' => GameTeamType::class,
                'entry_options' => array('label' => false),
                'label' => false
            ))
            ->getForm();

        return $form;
    }

    private function optionallySetWinner($form, $game)
    {
        if (null !== $form['winner']) {
            foreach ($game->getGameTeams() as $gameTeam) {
                if ($gameTeam->getId() == $form['winner']->getData()->getId()) {
                    $gameTeam->setResultCode(GameTeam::RESULT_WINNER);
                } else {
                    $gameTeam->setResultCode(GameTeam::RESULT_LOSER);
                }
            }
            $game->setStatus(Game::STATUS_ENDED);
        }
    }

    private function optionallyUpdateNextGames($form, $game)
    {
        if (null !== $form['winner'] && null !== $game->getParent()) {
            $winnerGameTeam = $form['winner']->getData();
            $winnerTournamentMembership = $winnerGameTeam->getTournamentMembership();
            $nextGame = $game->getParent();
            $newGameTeam = new GameTeam();
            $newGameTeam->setGame($nextGame);
            $newGameTeam->setTournamentMembership($winnerTournamentMembership);
            $nextGame->addGameTeam($newGameTeam);
        }
    }

//    /**
//     * Deletes a game entity.
//     *
//     * @Route("/{id}", name="game_delete")
//     * @Method("DELETE")
//     */
//    public function deleteAction(Request $request, Game $game)
//    {
//        $form = $this->createDeleteForm($game);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->remove($game);
//            $em->flush();
//        }
//
//        return $this->redirectToRoute('game_index');
//    }

//    /**
//     * Creates a form to delete a game entity.
//     *
//     * @param Game $game The game entity
//     *
//     * @return \Symfony\Component\Form\Form The form
//     */
//    private function createDeleteForm(Game $game)
//    {
//        return $this->createFormBuilder()
//            ->setAction($this->generateUrl('game_delete', array('id' => $game->getId())))
//            ->setMethod('DELETE')
//            ->getForm()
//        ;
//    }
}
