<?php

namespace TournamentBundle\Controller;

use TournamentBundle\Entity\Team;
use TournamentBundle\Entity\TeamMembership;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Team controller.
 *
 * @Route("team")
 */
class TeamController extends Controller
{
    /**
     * Lists all team entities.
     *
     * @Route("/", name="team_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $teams = $em->getRepository('TournamentBundle:Team')->findAll();

        return $this->render('team/index.html.twig', array(
            'teams' => $teams,
            'userTeamIds' => $this->getUserTeamIds()
        ));
    }

    /**
     * Lists all teams managed by logged user.
     *
     * @Route("/managed", name="team_managed")
     * @Method("GET")
     */
    public function allManagedTeamsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $teams = $em->getRepository('TournamentBundle:Team')->findManagedTeams($user);

        return $this->render('team/managed.html.twig', array(
            'teams' => $teams,
            'userTeamIds' => $this->getUserTeamIds()
        ));
    }

    /**
     * Lists all teams joined by logged user.
     *
     * @Route("/joined", name="team_joined")
     * @Method("GET")
     */
    public function allJoinedTeamsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $teams = $em->getRepository('TournamentBundle:Team')->findJoinedTeams($user);

        return $this->render('team/joined.html.twig', array(
            'teams' => $teams,
            'userTeamIds' => $this->getUserTeamIds()
        ));
    }

    /**
     * Creates a new team entity.
     *
     * @Route("/new", name="team_new")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function newAction(Request $request)
    {
        $team = new Team();
        $form = $this->createForm('TournamentBundle\Form\TeamType', $team);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (null !== $team->getLogo()) {
                $file = $team->getLogo();
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('photos_directory'),
                    $fileName
                );
                $team->setLogo($fileName);
            }
            $team->setManager($this->getUser());
            $team->setDateStart(new \DateTime());


            $em = $this->getDoctrine()->getManager();
            $em->persist($team);
            $em->flush();

            return $this->redirectToRoute('team_show', array('id' => $team->getId()));
        }

        return $this->render('team/new.html.twig', array(
            'team' => $team,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a team entity.
     *
     * @Route("/{id}", name="team_show")
     * @Method("GET")
     */
    public function showAction(Team $team)
    {
        $deleteForm = $this->createDeleteForm($team);

        return $this->render('team/show.html.twig', array(
            'team' => $team,
            'delete_form' => $deleteForm->createView(),
            'userTeamIds' => $this->getUserTeamIds()
        ));
    }

    /**
     * Displays a form to edit an existing team entity.
     *
     * @Route("/{id}/edit", name="team_edit")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function editAction(Request $request, Team $team)
    {
        $userIsTeamManager = $this->getUser()->getId() === $team->getManager()->getId();
        if (!$userIsTeamManager) {
            return $this->redirectToRoute('team_show', array('id' => $team->getId()));
        }

        if (null !== $team->getLogo()) {
            $team->setLogo(
                new File($this->getParameter('photos_directory').'/'.$team->getLogo())
            );
        }

        $deleteForm = $this->createDeleteForm($team);
        $editForm = $this->createForm('TournamentBundle\Form\TeamType', $team);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if (null !== $team->getLogo()) {
                $file = $team->getLogo();
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('photos_directory'),
                    $fileName
                );
                $team->setLogo($fileName);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('team_edit', array('id' => $team->getId()));
        }

        return $this->render('team/edit.html.twig', array(
            'team' => $team,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a team entity.
     *
     * @Route("/{id}", name="team_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteAction(Request $request, Team $team)
    {
        $userIsTeamManager = $this->getUser()->getId() === $team->getManager()->getId();
        if (!$userIsTeamManager) {
            return $this->redirectToRoute('team_show', array('id' => $team->getId()));
        }

        $form = $this->createDeleteForm($team);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($team);
            $em->flush();
        }

        return $this->redirectToRoute('team_index');
    }


    /**
     * Creates a form to delete a team entity.
     *
     * @param Team $team The team entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Team $team)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('team_delete', array('id' => $team->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @Route("/{id}/join", name="team_join")
     * @Method({"GET"})
     * @Security("has_role('ROLE_USER')")
     */
    public function joinTeamAction(Team $teamToJoin)
    {
        $userTeamIds = $this->getUserTeamIds();
        if (in_array($teamToJoin->getId(), $userTeamIds)) {
            return $this->redirectToRoute('team_show', array('id' => $teamToJoin->getId()));
        }

        $newTeamMembership = new TeamMembership();
        $newTeamMembership->setTeam($teamToJoin);
        $newTeamMembership->setUser($this->getUser());
        $newTeamMembership->setDateStart(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($newTeamMembership);
        $em->flush();
        return $this->redirectToRoute('team_show', array('id' => $teamToJoin->getId()));
    }

    private function getUserTeamIds()
    {
        $userTeamIds = [];
        $user = $this->getUser();
        if ($user) {
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('TournamentBundle:TeamMembership');
            $teamMemberships = $repository->findByUser($user);
            foreach ($teamMemberships as $teamMembership) {
                $userTeamIds[] = $teamMembership->getTeam()->getId();
            }
        }

        return $userTeamIds;
    }
}
