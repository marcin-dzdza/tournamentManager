<?php

namespace TournamentBundle\Controller;

use TournamentBundle\Entity\GameTeam;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Gameteam controller.
 *
 * @Route("gameteam")
 */
class GameTeamController extends Controller
{
    /**
     * Lists all gameTeam entities.
     *
     * @Route("/", name="gameteam_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $gameTeams = $em->getRepository('TournamentBundle:GameTeam')->findAll();

        return $this->render('gameteam/index.html.twig', array(
            'gameTeams' => $gameTeams,
        ));
    }

    /**
     * Creates a new gameTeam entity.
     *
     * @Route("/new", name="gameteam_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $gameTeam = new Gameteam();
        $form = $this->createForm('TournamentBundle\Form\GameTeamType', $gameTeam);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($gameTeam);
            $em->flush();

            return $this->redirectToRoute('gameteam_show', array('id' => $gameTeam->getId()));
        }

        return $this->render('gameteam/new.html.twig', array(
            'gameTeam' => $gameTeam,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a gameTeam entity.
     *
     * @Route("/{id}", name="gameteam_show")
     * @Method("GET")
     */
    public function showAction(GameTeam $gameTeam)
    {
        $deleteForm = $this->createDeleteForm($gameTeam);

        return $this->render('gameteam/show.html.twig', array(
            'gameTeam' => $gameTeam,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing gameTeam entity.
     *
     * @Route("/{id}/edit", name="gameteam_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, GameTeam $gameTeam)
    {
        $deleteForm = $this->createDeleteForm($gameTeam);
        $editForm = $this->createForm('TournamentBundle\Form\GameTeamType', $gameTeam);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gameteam_edit', array('id' => $gameTeam->getId()));
        }

        return $this->render('gameteam/edit.html.twig', array(
            'gameTeam' => $gameTeam,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a gameTeam entity.
     *
     * @Route("/{id}", name="gameteam_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, GameTeam $gameTeam)
    {
        $form = $this->createDeleteForm($gameTeam);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($gameTeam);
            $em->flush();
        }

        return $this->redirectToRoute('gameteam_index');
    }

    /**
     * Creates a form to delete a gameTeam entity.
     *
     * @param GameTeam $gameTeam The gameTeam entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(GameTeam $gameTeam)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('gameteam_delete', array('id' => $gameTeam->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
