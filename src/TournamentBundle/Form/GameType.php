<?php

namespace TournamentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class GameType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $builder->add('winner', EntityType::class, array(
//            'property_path' => false,
//            'class' => 'TournamentBundle:GameTeam',
//            'query_builder' => function(EntityRepository $er) {
//                return $er->createQueryBuilder('')
//                    ->select('gt')
//                    ->from('GameTeam', 'gt')
//                    ->where('gt.game = ?1')
//                    ->setParameter(1, )
//                    ->groupBy('u.id')
//                    ->orderBy('u.name', 'ASC');
//            }
//        ))
//            ->add('gameTeams', CollectionType::class, array(
//            'entry_type' => GameTeamType::class,
//            'entry_options' => array(),
//        ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TournamentBundle\Entity\Game'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'tournamentbundle_game';
    }
}