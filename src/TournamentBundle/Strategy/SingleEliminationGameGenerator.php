<?php

namespace TournamentBundle\Strategy;

use TournamentBundle\Entity\Tournament;

class SingleEliminationGameGenerator implements GameGenerator
{
    private $tournament;

    public function __construct(Tournament $tournament)
    {
        $this->tournament = $tournament;
    }

    public function generateGames()
    {
        $teams = $this->tournament->getTournamentMemberships()->getValues();
        $algorithm = new SingleEliminationGameGeneratorAlgorithm($teams, $this->tournament);
        $parentOfAllGamesWithChildren = $algorithm->generateGames();

        $this->tournament->setStatus(Tournament::STATUS_STARTED);

        return $parentOfAllGamesWithChildren;
    }
}