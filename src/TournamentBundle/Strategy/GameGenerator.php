<?php

namespace TournamentBundle\Strategy;

use TournamentBundle\Entity\Tournament;

/**
 * Game generating strategy, based on strategy design pattern
 */
interface GameGenerator
{

    /**
     * GameGenerator constructor.
     * @param Tournament $tournament
     */
    public function __construct(Tournament $tournament);

    /**
     * @param Tournament $tournament
     * @return Game[]|null
     */
    public function generateGames();
}