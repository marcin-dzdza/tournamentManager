<?php
namespace TournamentBundle\Strategy;

use TournamentBundle\Entity\Game;
use TournamentBundle\Entity\GameTeam;
use TournamentBundle\Entity\Tournament;

class SingleEliminationGameGeneratorAlgorithm
{
    private $teamsNotAssignedYet;
    private $tournament;
    private $roundCount;
    private $byeCount;
    private $byesNotAssignedYet;
    private $doubleByeGameCount;
    private $doubleByeGamesNotAssignedYet;

    public function __construct($teams, $tournament = null)
    {
        $this->teamsNotAssignedYet = $teams;
        $notEnoughTeams = count($this->teamsNotAssignedYet) < 2;
        if ($notEnoughTeams) {
            throw new \Exception('not enough teams');
        }

        $this->tournament = $tournament;
    }

    public function generateGames()
    {
        $this->computeRoundCount();
        $this->computeByeCount();
        $this->computeDoubleByeGameCount();
        $currentRound = $this->roundCount;
        $parentOfAllGamesWithChildren = $this->generateGameWithChildGames($currentRound);

        return $parentOfAllGamesWithChildren;
    }

    private function computeRoundCount()
    {
        $rounds = 0;

        for (;;) {
            $rounds++;
            if (pow(2, $rounds) >= count($this->teamsNotAssignedYet)) {
                break;
            }
        }

        $this->roundCount = $rounds;
    }

    private function computeByeCount()
    {
        $idealNumberOfTeamsInLastRound = pow(2, $this->roundCount);
        $actualNumberOfTeams = count($this->teamsNotAssignedYet);
        $this->byeCount = $idealNumberOfTeamsInLastRound - $actualNumberOfTeams;
        $this->byesNotAssignedYet = $this->byeCount;
    }

    private function computeDoubleByeGameCount()
    {
        $idealNumberOfTeamsInNextToLastRound = pow(2, $this->roundCount - 1);
        $numberOfGamesInNextToLastRound = $idealNumberOfTeamsInNextToLastRound / 2;

        $freeSlots = $idealNumberOfTeamsInNextToLastRound - $this->byeCount;
        $doubleByeGameCount = $numberOfGamesInNextToLastRound - $freeSlots;
        if ($doubleByeGameCount < 0) {
            $doubleByeGameCount = 0;
        }

        $this->doubleByeGameCount = $doubleByeGameCount;
        $this->doubleByeGamesNotAssignedYet = $this->doubleByeGameCount;
    }

    private function generateGameWithChildGames(int $currentRound, $currentParent = null)
    {
        $game = $this->createGameWithoutTeamsAndChildren($currentRound, $currentParent);

        if ($currentRound == 1) {
            for ($i = 0; $i < 2; $i++) {
                if (count($this->teamsNotAssignedYet) > 0) {
                    $game->addGameTeam($this->extractTournamentTeamAndCreateGameTeam($game));
                }
            }

            return $game;
        }

        if ($currentRound == 2 && $this->byesNotAssignedYet > 0) {
            $game->addGameTeam($this->extractTournamentTeamAndCreateGameTeam($game));
            $this->byesNotAssignedYet--;
            if ($this->doubleByeGamesNotAssignedYet > 0) {
                $game->addGameTeam($this->extractTournamentTeamAndCreateGameTeam($game));
                $this->byesNotAssignedYet--;
                $this->doubleByeGamesNotAssignedYet--;
            }
        }

        $numberOfChildGames = 2 - count($game->getGameTeams());

        for ($i = 0; $i < $numberOfChildGames; $i++) {
            $game->addChild($this->generateGameWithChildGames(
                $currentRound - 1,
                $game
            ));
        }

        return $game;
    }

    private function createGameWithoutTeamsAndChildren($currentRound, $currentParent)
    {
        $game = new Game();
        $game->setLevel($currentRound);
        $game->setStatus(Game::STATUS_PLANNED);
        if (isset($this->tournament)) {
            $game->setTournament($this->tournament);
        }
        if (isset($currentParent)) {
            $game->setParent($currentParent);
        }

        return $game;
    }

    private function extractTournamentTeamAndCreateGameTeam($game)
    {
        $tournamentTeam = $this->extractRandomTeamNotAssignedYet();

        if (!isset($this->tournament)) {
            return $tournamentTeam;
        }

        $gameTeam = new GameTeam();
        $gameTeam->setTournamentMembership($tournamentTeam);
        $gameTeam->setGame($game);

        return $gameTeam;
    }

    private function extractRandomTeamNotAssignedYet()
    {

        $numberOfTeamsNotAssignedYet = count($this->teamsNotAssignedYet);
        $randomNumber = mt_rand(0, $numberOfTeamsNotAssignedYet - 1);
        $randomTeam = array_splice($this->teamsNotAssignedYet, $randomNumber, 1)[0];
        return $randomTeam;
    }

    public function getRoundCount()
    {
        return $this->roundCount;
    }

    public function getByeCount()
    {
        return $this->byeCount;
    }

    public function getDoubleByeGameCount()
    {
        return $this->doubleByeGameCount;
    }
}