<?php

namespace TournamentBundle\Strategy;

use TournamentBundle\Entity\Tournament;

class ManualGameGenerator implements GameGenerator
{
    private $tournament;

    public function __construct(Tournament $tournament)
    {
        $this->tournament = $tournament;
    }

    public function generateGames()
    {
        return [];
    }
}
