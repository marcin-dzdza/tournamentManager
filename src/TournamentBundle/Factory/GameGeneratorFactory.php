<?php

namespace TournamentBundle\Factory;

use TournamentBundle\Entity\Tournament;
use TournamentBundle\Strategy\ManualGameGenerator;
use TournamentBundle\Strategy\SingleEliminationGameGenerator;

/**
 * Factory for objects implementing GameGenerator interface
 */
class GameGeneratorFactory
{
    static public function create(Tournament $tournament)
    {
        switch ($tournament->getSystem()) {
            case Tournament::SYSTEM_SINGLE_ELIMINATION:
                $gameGenerator = new SingleEliminationGameGenerator($tournament);
                break;
            default:
                $gameGenerator = new SingleEliminationGameGenerator($tournament);
        }

        return $gameGenerator;
    }
}