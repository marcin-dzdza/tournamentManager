<?php
namespace TournamentBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "app/console")
            ->setName('marcin:hello')

            // the short description shown while running "php app/console list"
            ->setDescription('Pisze hello')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Nie ma pomocy');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write('Hello');
    }
}