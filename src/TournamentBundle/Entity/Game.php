<?php

namespace TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="TournamentBundle\Repository\GameRepository")
 */
class Game
{
    const STATUS_PLANNED = 10;
    const STATUS_ENDED = 30;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="integer")
     */
    private $level;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Game", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="parent", cascade={"persist"})
     */
    private $children;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="games")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    private $tournament;

    /**
     * @ORM\OneToMany(targetEntity="GameTeam", mappedBy="game", cascade={"persist"})
     */
    private $gameTeams;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->gameTeams = new ArrayCollection();
    }

    public function getWinner()
    {
        foreach ($this->getGameTeams() as $team) {
            if ($team->getResultCode() == GameTeam::RESULT_WINNER) {
                return $team;
            }
        }
    }

    public function getStatusName($statusCode)
    {
        switch ($statusCode) {
            case self::STATUS_PLANNED:
                $statusName = 'planned';
                break;
            case self::STATUS_ENDED:
                $statusName = 'ended';
                break;
            default:
                $statusName = 'Invalid status code';
        }

        return $statusName;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Game
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return Game
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Game
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * @param int $tournament
     */
    public function setTournament(Tournament $tournament)
    {
        $this->tournament = $tournament;
    }

    /**
     * @return int
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param int $parent
     */
    public function setParent(Game $parent)
    {
        $this->parent = $parent;
    }

    public function addChild(Game $child)
    {
        $this->children[] = $child;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function addGameTeam($gameTeam)
    {
        $this->gameTeams[] = $gameTeam;
    }

    public function getGameTeams()
    {
        return $this->gameTeams;
    }

    public function getScoreString()
    {
        $defaultString = '?';

        $notEnoughTeams = count($this->getGameTeams()) < 2;
        if ($notEnoughTeams) {
            return $defaultString;
        }

        $gameNotEnded = $this->getStatus() != self::STATUS_ENDED;
        if ($gameNotEnded) {
            return $defaultString;
        }

        $team1score = $this->getGameTeams()[0]->getGameScore();
        $team2score = $this->getGameTeams()[1]->getGameScore();
        $string = $team1score . ' : ' . $team2score;

        return $string;
    }
}
