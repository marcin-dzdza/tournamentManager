<?php

namespace TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Tournament
 *
 * @ORM\Table(name="tournament")
 * @ORM\Entity(repositoryClass="TournamentBundle\Repository\TournamentRepository")
 */
class Tournament
{
    const STATUS_RECRUITING = 10;
    const STATUS_STARTED = 30;

    const SYSTEM_SINGLE_ELIMINATION = 20;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="system", type="integer")
     */
    private $system;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="datetime")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="managedTournaments")
     * @ORM\JoinColumn(name="manager_id", referencedColumnName="id")
     */
    private $manager;

    /**
     * @ORM\OneToMany(targetEntity="TournamentMembership", mappedBy="tournament")
     */
    private $tournamentMemberships;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="tournament")
     */
    private $games;

    public function __construct()
    {
        $this->tournamentMemberships = new ArrayCollection();
        $this->games = new ArrayCollection();
    }

    public function getSystemsArray()
    {
        $systemsArray = [];
        foreach ($this->getAllSystemValues() as $value) {
            $name = $this->getSystemName($value);
            $systemsArray[$value] = $name;
        }

        return $systemsArray;
    }

    public function getAllSystemValues()
    {
        $reflectionClass = new \ReflectionClass( __CLASS__ );
        $allConstants = $reflectionClass->getConstants();
        $systemConstants = array_filter(
            $allConstants,
            function ($key) {
                $systemPrefix = 'SYSTEM_';
                $constantBeginning = substr($key, 0, 7);
                return $constantBeginning === $systemPrefix;
            },
            ARRAY_FILTER_USE_KEY
        );
        return array_values($systemConstants);
    }

    public function getSystemName($system)
    {
        switch ($system) {
            case self::SYSTEM_SINGLE_ELIMINATION:
                $systemName = 'Single-elimination tournament';
                break;
            default:
                $systemName = 'Invalid system name';
        }

        return $systemName;
    }

    public function getStatusesArray()
    {
        $statusesArray = [];
        foreach ($this->getAllStatusValues() as $value) {
            $name = $this->getStatusName($value);
            $statusesArray[$value] = $name;
        }

        return $statusesArray;
    }

    public function getAllStatusValues()
    {
        $reflectionClass = new \ReflectionClass( __CLASS__ );
        $allConstants = $reflectionClass->getConstants();
        $statusConstants = array_filter(
            $allConstants,
            function ($key) {
                $systemPrefix = 'STATUS_';
                $constantBeginning = substr($key, 0, 7);
                return $constantBeginning === $systemPrefix;
            },
            ARRAY_FILTER_USE_KEY
        );
        return array_values($statusConstants);
    }

    public function getStatusName($status)
    {
        switch ($status) {
            case self::STATUS_RECRUITING:
                $statusName = 'Waiting for teams';
                break;
            case self::STATUS_STARTED:
                $statusName = 'Started';
                break;
            default:
                $statusName = 'Invalid status name';
        }

        return $statusName;
    }

    public function getFirstGameNodes()
    {
        $firstGameNodes = [];
        foreach ($this->games as $game) {
            if (false === $game->getParent() instanceof Game) {
                $firstGameNodes[] = $game;
            }
        }
        return $firstGameNodes;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Tournament
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Tournament
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set system
     *
     * @param integer $system
     * @return Tournament
     */
    public function setSystem($system)
    {
        $this->system = $system;

        return $this;
    }

    /**
     * Get system
     *
     * @return integer 
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Tournament
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return Tournament
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime 
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return Tournament
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime 
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @return int
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param int $manager
     */
    public function setManager($manager)
    {
        $this->manager = $manager;
    }

    public function addTournamentMembership(TournamentMembership $tournamentMembership)
    {
        $this->tournamentMemberships[] = $tournamentMembership;
    }

    public function getTournamentMemberships()
    {
        return $this->tournamentMemberships;
    }

    public function addGame(Game $game)
    {
        $this->games[] = $game;
    }

    public function getGames()
    {
        return $this->games;
    }
}
