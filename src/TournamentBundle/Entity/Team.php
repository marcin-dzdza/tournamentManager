<?php

namespace TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="TournamentBundle\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, options={"collation":"utf8_polish_ci"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true, options={"collation":"utf8_polish_ci"})
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=64, nullable=true)
     * @Assert\File(
     *     mimeTypes={ "image/jpeg", "image/png", "image/gif" },
     *     mimeTypesMessage = "Possible image extensions: jpeg, jpg, png, gif"
     * )
     */
    private $logo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="datetime")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="managedTeams")
     * @ORM\JoinColumn(name="manager_id", referencedColumnName="id")
     */
    private $manager;

    /**
     * @ORM\OneToMany(targetEntity="TeamMembership", mappedBy="team")
     */
    private $teamMemberships;

    /**
     * @ORM\OneToMany(targetEntity="TournamentMembership", mappedBy="team")
     */
    private $tournamentMemberships;

    public function __construct()
    {
        $this->teamMemberships = new ArrayCollection();
        $this->tournamentMemberships = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Team
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Team
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return Team
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime 
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return Team
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime 
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @return int
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param int $manager
     */
    public function setManager($manager)
    {
        $this->manager = $manager;
    }

    public function addTeamMembership(TeamMembership $teamMembership)
    {
        $this->teamMemberships[] = $teamMembership;
    }

    public function getTeamMemberships()
    {
        return $this->teamMemberships;
    }

    public function addTournamentMembership(TournamentMembership $tournamentMembership)
    {
        $this->tournamentMemberships[] = $tournamentMembership;
    }

    public function getTournamentMemberships()
    {
        return $this->tournamentMemberships;
    }
}
