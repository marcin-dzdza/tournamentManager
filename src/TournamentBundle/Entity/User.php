<?php

namespace TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="TournamentBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true, options={"collation":"utf8_polish_ci"})
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=64, nullable=true)
     * @Assert\File(
     *     mimeTypes={ "image/jpeg", "image/png", "image/gif" },
     *     mimeTypesMessage = "Possible image extensions: jpeg, jpg, png, gif"
     * )
     */
    private $photo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * @ORM\OneToMany(targetEntity="Team", mappedBy="manager")
     */
    private $managedTeams;

    /**
     * @ORM\OneToMany(targetEntity="TeamMembership", mappedBy="user")
     */
    private $teamMemberships;

    /**
     * @ORM\OneToMany(targetEntity="Tournament", mappedBy="manager")
     */
    private $managedTournaments;

    public function __construct()
    {
        parent::__construct();

        $this->managedTeams = new ArrayCollection();
        $this->teamMemberships = new ArrayCollection();
        $this->managedTournaments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return User
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return User
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string 
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return User
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function addManagedTeam(Team $team)
    {
        $this->managedTeams[] = $team;
    }

    public function getManagedTeams()
    {
        return $this->managedTeams;
    }

    public function addTeamMembership(TeamMembership $teamMembership)
    {
        $this->teamMemberships[] = $teamMembership;
    }

    public function getTeamMemberships()
    {
        return $this->teamMemberships;
    }

    public function addManagedTournament(Tournament $tournament)
    {
        $this->managedTournaments[] = $tournament;
    }

    public function getManagedTournaments()
    {
        return $this->managedTournaments;
    }
}
