<?php

namespace TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * GameTeam
 *
 * @ORM\Table(name="game_team")
 * @ORM\Entity(repositoryClass="TournamentBundle\Repository\GameTeamRepository")
 */
class GameTeam
{
    const RESULT_WINNER = 10;
    const RESULT_LOSER = 20;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="game_score", type="string", nullable=true, length=80, options={"collation":"utf8_polish_ci"})
     */
    private $gameScore;

    /**
     * @var int
     *
     * @ORM\Column(name="tournament_score", type="integer", nullable=true)
     */
    private $tournamentScore;

    /**
     * @var int
     *
     * @ORM\Column(name="resultCode", type="integer", nullable=true)
     */
    private $resultCode;

    /**
     * @ORM\ManyToOne(targetEntity="Game", inversedBy="gameTeams")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     */
    private $game;

    /**
     * @ORM\ManyToOne(targetEntity="TournamentMembership", inversedBy="gameTeams")
     * @ORM\JoinColumn(name="tournament_membership_id", referencedColumnName="id")
     */
    private $tournamentMembership;

    public function getResultCodesArray()
    {
        $resultsArray = [];
        foreach ($this->getAllResultCodeValues() as $value) {
            $name = $this->getResultName($value);
            $resultsArray[$value] = $name;
        }

        return $resultsArray;
    }

    public function getAllResultCodeValues()
    {
        $reflectionClass = new \ReflectionClass( __CLASS__ );
        $allConstants = $reflectionClass->getConstants();
        $resultConstants = array_filter(
            $allConstants,
            function ($key) {
                $systemPrefix = 'RESULT_';
                $constantBeginning = substr($key, 0, 7);
                return $constantBeginning === $systemPrefix;
            },
            ARRAY_FILTER_USE_KEY
        );
        return array_values($resultConstants);
    }

    public function getResultName($resultCode)
    {
        switch ($resultCode) {
            case self::RESULT_WINNER:
                $resultName = 'winner';
                break;
            case self::RESULT_LOSER:
                $resultName = 'loser';
                break;
            default:
                $resultName = 'Invalid result code';
        }

        return $resultName;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gameScore
     *
     * @param string $gameScore
     * @return GameTeam
     */
    public function setGameScore($gameScore)
    {
        $this->gameScore = $gameScore;

        return $this;
    }

    /**
     * Get gameScore
     *
     * @return string
     */
    public function getGameScore()
    {
        return $this->gameScore;
    }

    /**
     * Set tournamentScore
     *
     * @param integer $tournamentScore
     * @return GameTeam
     */
    public function setTournamentScore($tournamentScore)
    {
        $this->tournamentScore = $tournamentScore;

        return $this;
    }

    /**
     * Get tournamentScore
     *
     * @return integer 
     */
    public function getTournamentScore()
    {
        return $this->tournamentScore;
    }

    /**
     * Set resultCode
     *
     * @param integer $resultCode
     * @return GameTeam
     */
    public function setResultCode($resultCode)
    {
        $this->resultCode = $resultCode;

        return $this;
    }

    /**
     * Get resultCode
     *
     * @return integer 
     */
    public function getResultCode()
    {
        return $this->resultCode;
    }

    /**
     * @return mixed
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * @param mixed $game
     */
    public function setGame(Game $game)
    {
        $this->game = $game;
    }

    /**
     * @return mixed
     */
    public function getTournamentMembership()
    {
        return $this->tournamentMembership;
    }

    /**
     * @param mixed $tournamentMembership
     */
    public function setTournamentMembership(TournamentMembership $tournamentMembership)
    {
        $this->tournamentMembership = $tournamentMembership;
    }


}
