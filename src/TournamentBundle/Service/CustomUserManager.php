<?php

namespace TournamentBundle\Service;

use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CustomUserManager extends UserManager
{

    /**
     * {@inheritdoc}
     */
    public function updateUser(UserInterface $user, $andFlush = true)
    {
        $this->updateCanonicalFields($user);
        $this->updatePassword($user);

        //start of added code
        if (empty($user->getDateCreated())) {
            $user->setDateCreated(new \DateTime());
        }
        //end of added code

        $this->objectManager->persist($user);
        if ($andFlush) {
            $this->objectManager->flush();
        }
    }
}